<?php

namespace testsunit;

use credy\kz\PersonalidValidator;
use yii\base\Model;

/**
 * Class PersonalidValidatorTest
 *
 * @package tests\unit
 */
class PersonalidValidatorTest extends \Codeception\Test\Unit
{
    /**
     * @var \tests\UnitTester
     */
    public $tester;

    /**
     * @dataProvider validationDataProvider
     */
    public function testValidation($value, $error)
    {
        $validator = new PersonalidValidator();
        $validator->message = 'Custom message';
        $this->assertEquals($error, $validator->validateValue($value));
    }

    /**
     * @return array
     */
    public function validationDataProvider()
    {
        return [
            'empty' => ['', null],
            'integer' => [850407101163, null],
            // 12 symbols
            'too short' => ['12345678901', ['Custom message', []]],
            'too long' => ['1234567890123', ['Custom message', []]],
            // numeric
            'only characters' => ['abcdefghijkl', ['Custom message', []]],
            'contains characters' => ['123456a89012', ['Custom message', []]],
            // checksum
            'wrong checksum' => ['850407401163', ['Custom message', []]],
            // century flag
            'not existing century' => ['850407001167', ['Custom message', []]],
            '18xx male' => ['850407101163', null],
            '18xx female' => ['850407201161', null],
            '19xx male' => ['850407301166', null],
            '19xx female' => ['850407401162', null],
            '20xx male' => ['850407501169', null],
            '20xx female' => ['850407601165', null],
            '21xx male' => ['850407701161', ['Custom message', []]],
            '21xx female' => ['850407801168', ['Custom message', []]],
            'will not work that far in future' => ['850407901164', ['Custom message', []]],
            'value is array' => [['value' => '850407901164'], ['Custom message', []]],
        ];
    }

    /**
     * @test
     */
    public function clientValidateAttribute()
    {
        $validator = new PersonalidValidator();
        $js = $validator->clientValidateAttribute(new Model(), 'attribute', 'view');
        $this->tester->assertNotEmpty($js, 'There should be some JS for client validation');
    }
}
